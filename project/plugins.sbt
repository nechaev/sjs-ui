addSbtPlugin("org.portable-scala" % "sbt-scalajs-crossproject" % "0.6.0")
addSbtPlugin("org.scala-js"       % "sbt-scalajs"              % "0.6.25")
addSbtPlugin("ch.epfl.scala"      % "sbt-scalajs-bundler"      % "0.13.1")
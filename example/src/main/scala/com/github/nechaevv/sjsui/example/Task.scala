package com.github.nechaevv.sjsui.example

case class Task(name: String, completed: Boolean)
